package com.ipiecoles.java.eval.mdd050.controller;

import com.ipiecoles.java.eval.mdd050.model.Artist;
import com.ipiecoles.java.eval.mdd050.repository.ArtistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@RestController
@RequestMapping("/artists")
public class ArtistController {

    @Autowired
    private ArtistRepository artistRepo;


    // GET ONE ARTIST BY ID
    @GetMapping("/{id}")
    public Optional<Artist> getArtist(@PathVariable(value="id") Long id){
        Optional<Artist> artist = artistRepo.findById(id);
        if(!artist.isPresent()){
            throw new EntityNotFoundException("Impossible de trouver l'artiste d'identifiant " + id);
        }
        return artist;
    }

    //GET ONE ARTIST BY SEARCH BY NAME
    @GetMapping(params = "name")
    public Page<Artist> searchArtistByName(@RequestParam(value="name") String name,
                                           @RequestParam(value = "page", defaultValue = "0") Integer page,
                                           @RequestParam(value = "size", defaultValue = "10") Integer size,
                                           @RequestParam(value = "sortProperty", defaultValue = "name") String sortProperty,
                                           @RequestParam(value = "sortDirection", defaultValue = "ASC") Sort.Direction sortDirection){

        Pageable pageable = PageRequest.of(page, size, sortDirection, sortProperty);

        return  artistRepo.findByNameContainingIgnoreCase(name,pageable);
    }


    //GET ALL ARTIST WITH PAGING AND SORTING

    @GetMapping
    public Page<Artist> getAllArtist(
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "10") Integer size,
            @RequestParam(value = "sortProperty", defaultValue = "name") String sortProperty,
            @RequestParam(value = "sortDirection", defaultValue = "ASC") Sort.Direction sortDirection

    ){
        Pageable pageable = PageRequest.of(page, size, sortDirection, sortProperty);

        return artistRepo.findAll(pageable);
    }

    // CREATE ARTIST WITH POST
    @PostMapping
    public Artist createArtist(
            @RequestBody Artist artist){
        Boolean exists = artistRepo.findByNameContainingIgnoreCase(artist.getName());
        if(exists) {
            throw new EntityExistsException("L'artiste  " + artist.getName() + " existe déjà !");
        }
        return artistRepo.save(artist);
    }

    // UPDATE AN ARTIST
    @PutMapping("/{id}")
    public Artist updateArtist(@PathVariable(value="id")Long id, @RequestBody Artist artist){
        return artistRepo.save(artist);
    }

    //DELETE AN ARTIST
    @DeleteMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteArtist(@PathVariable(value="id") Long id){

        //ajout dans le modele Artist
        //@OneToMany(orphanRemoval=true,mappedBy = "artist")
        artistRepo.deleteById(id);
    }




}
