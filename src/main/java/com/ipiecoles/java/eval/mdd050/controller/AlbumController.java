package com.ipiecoles.java.eval.mdd050.controller;


import com.ipiecoles.java.eval.mdd050.model.Album;
import com.ipiecoles.java.eval.mdd050.model.Artist;
import com.ipiecoles.java.eval.mdd050.repository.AlbumRepository;
import com.ipiecoles.java.eval.mdd050.repository.ArtistRepository;
import com.ipiecoles.java.eval.mdd050.service.AlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/albums")
public class AlbumController {


    @Autowired
    private AlbumRepository albumRepo;
    @Autowired
    private AlbumService albumService;

    //ajout album de artist
    @PostMapping
    public Album createAlbum(
            @RequestBody Album album) {
        return this.albumService.creerAlbum(album);
    }

    //delete album de artist
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteAlbum(
            @PathVariable("id") Long id) {
        this.albumService.deleteAlbum(id);
    }
}
